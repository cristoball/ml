# ML-DS fundamentals

## What?

Provide some fundamentals of Machine Learning and Data Science.  
The idea is to go through data visualization, analysis and apply machine learning on top of that.

## Why?

As a future graduated student in AI, I'm trying to learn more and catch up with some mathematical and machine-learning concept. I realized that it might contribute to others. So I'm making my notes and researches available online for anyone who might come across the same questions as I do.  

In order to make content research less painful in the repo, I try to organize my courses as one pdf file (including pictures), but also leaving chapters available separately via the markdown interpreter of git.

## How?

Along with the courses, I try to explain as accurate as possible how to visualize the data, how to interpret it and the mathematical concepts and intuitions behind algorithms used in pratice.  
I will sometimes go back to the basics to really grasp the meaning of what we are manipulating. Detailed code samples in python along with the chapters are uploaded.