import pandas as pd
import numpy as np
from math import log
import random

def extract_sentences(text):
    return clean_string(text).split(". ")


def clean_string(text):
    return text.lower()


class Tokenizer:

    def __init__(self, data):
        self.data = data

    def tokenize(self):
        return [sentence.lower().split() for sentence in self.data]


class TfIdf(object):

    def __init__(self, data):
        self.voc = None
        self.avg_len = None
        self.data = extract_sentences(data)
        self.raw_data = extract_sentences(data)
        self.rep_hot = None
        self.tf = None
        self.idf = None

    def voc_extraction(self):
        tokenizer = Tokenizer(self.data)
        sentences = tokenizer.tokenize()
        words_seen = []
        for sentence in sentences:
            for word in sentence:
                if word not in words_seen:
                    words_seen.append(word)
        self.data = sentences
        self.voc = words_seen
        self.avg_len = sum([len(sentence) for sentence in sentences]) / len(sentences)

    def one_hot(self):
        one_hot_data = np.zeros((len(self.data), len(self.voc)))
        num_sentence = 0
        try:
            for sentence in self.data:
                for word in sentence:
                    one_hot_data[num_sentence][self.voc.index(word)] += 1
                num_sentence += 1
            self.rep_hot = pd.DataFrame(columns=self.voc.copy(), data=one_hot_data, index=self.raw_data.copy())
        except ValueError:
            raise Exception("Word not in vocabulary")

    def term_frequencies(self):
        self.tf = self.rep_hot.copy()
        for col in self.tf.columns:
            self.tf[col] = self.tf[col] / sum(self.tf[col])

    def inverse_document_frequencies(self):
        self.idf = self.tf.copy()
        nb_documents = self.idf.shape[0]
        for col in self.idf.columns:
            nb_term_appearance = self.idf[self.idf[col] != 0].shape[0]
            self.idf[col] = self.idf[col] * log(nb_documents/nb_term_appearance)

    def execute(self):
        self.voc_extraction()
        self.one_hot()
        self.term_frequencies()
        self.inverse_document_frequencies()

    def summary(self):
        print("Vocabulary")
        print(self.voc)
        print("\n############################################\n")
        print("Appearance matrix")
        print(self.rep_hot)
        print("\n############################################\n")
        print("TF")
        print("Term frequency calculated across all documents")
        print(self.tf)
        print("\n############################################\n")
        print("IDF")
        print("Formula : term_frequency * log(number of documents / number of documents containing the term)")
        print(self.idf)

    def search(self, query):
        query = clean_string(query).split(" ")

        return [(doc, self.score(doc, query)) for doc in self.raw_data]


    def score(self, doc, query):
        k1 = random.uniform(1.2, 2.0)
        b = 0.75

        score = 0

        # TODO exctract in function tf and idf value access
        # TODO put k1 and b in class parameters
        for term in query:
            s = self.idf[term][doc] * self.tf[term][doc] * (k1 + 1) / (self.tf[term][doc] + k1 * (1 - b + b * len(doc) / self.avg_len))
            score =+ s

        return score

    def print_query_result(self, query):
        print("Searching for \"" + str(query) + "\"")
        print(sorted(self.search(query), key=lambda x: x[1], reverse=True))


if __name__ == '__main__':
    text = "This is a text. I could say my text"
    tfidf = TfIdf(data=text)
    tfidf.execute()
    tfidf.summary()
    tfidf.print_query_result("text is")

