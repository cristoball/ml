# Machine Learning project overview

In this section, we will cover the main aspects of what makes a well structured ML project.

## Data retrieval

### Find datasets

In general, instead of looking up a specific source, you may want to search for dataset across many sites. Google came to help with their dataset search engine, available [here](https://toolbox.google.com/datasetsearch).

Most common sources to find real-world data:
- UC Irvine Machine Learning Repository
- Kaggle datasets
- Amazon’s AWS datasets
- Wikipedia’s list of Machine Learning datasets
- Quora.com question
- Datasets subreddit

## Understand the problem

"Where there is a problem, there is a solution!" you might have heard this quote before. 

Well, solving a problem in ML is mainly setting clear objectives. The first thing you must ask yourself is "Do I need machine-learning?". To find the answer to that question, you need to understand the end goal. What is the purpose of the work you are about to do   
You need to make sure how your work will be used and how your results should appear to benefit future users like business analyst etc. The more informations you get, the best decisions you'll take (just like a model). Once this step completed, it will guide you all the way through the process of finding the right algorithm, the performance measure, and the output type of your system. Remember also that having the perfect solution to a problem may not respect the production constraints such as time, resources, etc.  

## Understand the data

"No data, no model". Unfortunately, you might know everything there is to know about creating models and algorithms, __BUT__ without usable data, it will be impossible for you to create a model capable of reasoning on your data. One of the first task will be to get into the depth of your dataset. Does your data correspond to the problem? Does it need to be cleaned up? Does it contain usable informations? Are they rich or poor?

## Frame the problem

One of the biggest task you will have to focus on afterwards is choosing the algorithm according to your goal.
Is it a regression or classification problem, or something else ? Once this figured out, you will know whether you should go for a supervised, unsupervised or reinforcement learning algorithm.